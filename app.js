const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');

const isProduction = process.env.NODE_ENV === 'production';

mongoose.connect(process.env.MONGODB_URI, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

require('./models');

if (!isProduction) {
  mongoose.set('debug', true);
}

const app = express();

app.use(morgan('dev'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(require('./routes'));

const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');

app.use('/doc', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
app.use('', (req, res) => res.redirect('/doc'));

module.exports = app;
