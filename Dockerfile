FROM node:10-alpine

RUN mkdir /app
WORKDIR /app


COPY package*.json ./

RUN npm i
# If you are building your code for production
# RUN npm ci --only=production

COPY . .

CMD ["node", "server.js"]