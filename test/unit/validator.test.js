const Validator = require('../../utils/lib/validator');

describe('Date validation', () => {
  const dateValidator = new Validator({
    date: { type: 'date' },
  });
  it('Should be true', () => {
    expect(dateValidator.validate({ date: '2014-12-22' }).isSuccess).toBe(true);
  });
  it('Should be false', () => {
    expect(dateValidator.validate({ date: '2014-12-33' }).isSuccess).toBe(false);
  });
});

describe('Integer validation', () => {
  const numberValidator = new Validator({
    number: { type: 'integer' },
  });
  it('Should be true', () => {
    expect(numberValidator.validate({ number: 22 }).isSuccess).toBe(true);
  });
  it('Should be false', () => {
    expect(numberValidator.validate({ number: '2014-12-22' }).isSuccess).toBe(false);
  });
});