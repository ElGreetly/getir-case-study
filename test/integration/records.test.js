const request = require('supertest');
const app = require('../../app');

describe('Test records end-point', () => {
    it('Should response the POST method with 200 statusCode', () => {
        return request(app).post('/api/records').send({
            "startDate": "2016-01-26",
            "endDate": "2018-02-28",
            "minCount": 2700,
            "maxCount": 3000,
            "page": 6,
            "perPage": 100
        }).then(response => {
            expect(response.statusCode).toBe(200)
        });
    });
    it('Should response the POST method with 422 statusCode', () => {
        return request(app).post('/api/records').send({
            "startDate": "2016-01-26",
            "endDate": "2018-02-28",
            "minCount": "2700",
            "maxCount": 3000,
            "page": 6,
            "perPage": 100
        }).then(response => {
            expect(response.statusCode).toBe(422)
        });
    });
});