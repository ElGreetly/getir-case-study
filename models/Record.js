const mongoose = require('mongoose');

const RecordSchema = new mongoose.Schema({
  key: String,
  value: String,
  createdAt: Date,
  counts: Number,
});

mongoose.model('Record', RecordSchema);
