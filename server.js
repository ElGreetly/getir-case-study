const app = require('./app');
const logger = require('./utils/lib/logger');

const PORT = Number(process.env.PORT) || 3000;

// Start server
app.listen(PORT, () => {
  logger.info(`Server started on port ${PORT}`);
});
