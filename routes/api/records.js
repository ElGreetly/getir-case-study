const router = require('express').Router();
const { listAggregator, listValidator } = require('../../utils/api/records');

router.post('/records', listValidator,
  async (req, res) => {
    const { body } = req;
    const { records, page, perPage } = await listAggregator(body);

    // Check if there is an error
    if (records.code) {
      return res.status(500).json(records);
    }

    // Get the query total
    const totalRecords = records.total[0] ? records.total[0].count : 0;

    // Success response
    res.json({
      code: 0,
      msg: 'success',
      records: records.data,
      total: totalRecords,
      pageContext: {
        page,
        perPage,
        hasMorePage: page * perPage < totalRecords,
      },
    });
  });

module.exports = router;
