# Getir Case Study

Getir express assignment project it has one end-point with validation connected to a dummy DB
- Application live production environment [Getir Application](https://greetly.dev)

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Note
This instructions is tested on both linux and macOS

### Prerequisites
For local run
```
node version 10
npm
mongoDB
```

### Installing

For local run create a copy from `.env.example` file rename it to `.env` then run

```
npm i
npm run dev
```

## Running the tests

To run test use terminal to execute `npm test`

### Tests is divided into

#### Integration test
- For testing the system units interactions

#### Unit test
- To test the functionality of the system

### Code lint
The code is linted using eslint according to the `airbnb-base` standard

## Deployment

For Heroku deployment follow up with instruction here [Node.js Heroku app deployment](https://devcenter.heroku.com/articles/getting-started-with-nodejs)
Also make sure to add the environment variables to your Heroku app
- The Heroku `Procfile` is already committed within the repo

## Built With

* [Expressjs](https://expressjs.com/) - The web framework used

## Authors

* **Ahmed ElGreetly** - *Initial work* - [Greetly](https://github.com/ElGreetly)
