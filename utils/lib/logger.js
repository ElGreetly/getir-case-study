function log(msg, colorCode) {
  console.log(`\x1b[1;${colorCode}m${msg}\x1b[0m`);
}

const logger = {
  info(msg) {
    log(msg, 32);
  },
  error(msg) {
    log(msg, 31);
  },
};

module.exports = logger;
