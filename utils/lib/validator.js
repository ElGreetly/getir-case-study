const logger = require('./logger');

class Validator {
  constructor(schema) {
    this.schema = schema;
    this.msg = [];
    this.isSuccess = true;
    this.types = [
      'date',
      'integer',
    ];
    this.isTypes = Object.keys(schema).reduce((accumulator, key) => {
      if (!this.types.includes(schema[key].type)) {
        logger.error(`Validator: wrong type for ${key} param!`);
        process.exit(1);
      }
      return true;
    }, true);
  }

  /**
   * validator factory takes the params
   * match it with the given schema
   *
   * @param {object} params
   * @returns {object} - {isSuccess: boolean, msg: string[]}
   * @memberof Validator
   */
  validate(params) {
    this.params = params;
    Object.keys(this.schema).forEach((key) => {
      const paramObj = this.schema[key];
      if (paramObj.required && !params[key]) {
        this.msg.push(`${key} is required!`);
        this.isSuccess = false;
        return false;
      }
      if (params[key] && !this[`${paramObj.type}Validator`](key)) {
        this.isSuccess = false;
        return false;
      }
    });
    return {
      isSuccess: this.isSuccess,
      msg: this.msg,
    };
  }

  /**
   * Date Validator
   *
   * @param {*} key
   * @returns {boolean}
   * @memberof Validator
   */
  dateValidator(key) {
    const value = this.params[key];
    // Match the date format through regular expression
    if (this.dateRegexTest(value)) {
      const dateArr = value.split('-');
      const yy = Number(dateArr[0]);
      const mm = Number(dateArr[1]);
      const dd = Number(dateArr[2]);
      // Create list of days of a month [assume there is no leap year by default]
      this.listOfDays = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
      if (
        ((mm === 1 || mm > 2) && dd > this.listOfDays[mm - 1])
        || !this.leapYearValidation(yy, mm, dd)
      ) {
        this.msg.push(`${key} is wrong date`);
        return false;
      }
      return true;
    }
    this.msg.push(`${key} does not match date pattern yyyy-mm-dd, or year rang 1970-2099`);
    return false;
  }

  /**
   * Date regex validation according to
   * yyyy-mm-dd schema & date range 1970-2099
   *
   * @param {*} text
   * @returns {boolean}
   * @memberof Validator
   */
  dateRegexTest(text) {
    // eslint-disable-next-line no-useless-escape
    this.dateformat = /^(197?[0-9]|20[0-9]{2})\-(0?[1-9]|1[012])\-(0?[1-9]|[12][0-9]|3[01])$/;
    return this.dateformat.test(text);
  }


  /**
   * Validates date for month 2
   *
   * @param {number} yy
   * @param {number} mm
   * @param {number} dd
   * @returns {boolean}
   * @memberof Validator
   */
  // eslint-disable-next-line class-methods-use-this
  leapYearValidation(yy, mm, dd) {
    if (mm !== 2) return true;
    const lYear = (!(yy % 4) && yy % 100) || !(yy % 400);
    if ((lYear === false && dd >= 29) || (lYear === true && dd > 29)) {
      return false;
    }
    return true;
  }

  /**
   * Integers validator
   *
   * @param {*} key
   * @returns {boolean}
   * @memberof Validator
   */
  integerValidator(key) {
    const input = this.params[key];
    if (!/^[1-9]$|^[1-9][0-9]+$/.test(input) || typeof input !== 'number') {
      this.msg.push(`${key} is not integer`);
      return false;
    }
    return true;
  }
}

module.exports = Validator;
