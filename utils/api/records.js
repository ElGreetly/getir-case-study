const mongoose = require('mongoose');
const logger = require('../lib/logger');
const Validator = require('../lib/validator');

const Record = mongoose.model('Record');

/**
 * Get a list of records according
 * to the aggregation query
 *
 * @param {object} body
 * @param {date} startDate
 * @param {date} endDate
 * @param {number} minCount
 * @param {number} maxCount
 * @param {number} page
 * @param {number} perPage
 * @returns {object} - {records, page, perPage}
 */
async function listAggregator(body) {
  const page = body.page || 1;
  const $limit = body.perPage || 50;
  const $skip = page ? (page - 1) * $limit : 0;
  const $project = {
    _id: 0,
    key: 1,
    createdAt: 1,
    totalCount: {
      $sum: '$counts',
    },
  };
  const $match = {
    createdAt: {
      $gt: new Date(body.startDate),
      $lt: new Date(body.endDate),
    },
    totalCount: {
      $gt: body.minCount,
      $lt: body.maxCount,
    },
  };
  const $facet = {
    data: [{ $skip }, { $limit }],
    total: [{ $count: 'count' }],
  };
  const [records] = await Record.aggregate([{ $project }, { $match }, { $facet }])
    .catch(
      (err) => {
        logger.error(`${err.toString()}`);
        return [{
          code: 25,
          msg: 'Something went wrong!',
        }];
      },
    );
  return {
    records, page, perPage: $limit,
  };
}

/**
 * Validate list records request params
 *
 * @param {*} req
 * @param {*} res
 * @param {*} next
 * @returns
 */
function listValidator(req, res, next) {
  const { body } = req;
  const validationSchema = {
    startDate: { type: 'date', required: true },
    endDate: { type: 'date', required: true },
    minCount: { type: 'integer', required: true },
    maxCount: { type: 'integer', required: true },
    page: { type: 'integer' },
    perPage: { type: 'integer' },
  };
  const validator = new Validator(validationSchema).validate(body);
  if (!validator.isSuccess) {
    return res.status(422).json({
      code: 10,
      msg: validator.msg.reduce((accumulator, msg) => `${accumulator}${accumulator && ', '}${msg}`, ''),
    });
  }
  return next();
}

module.exports = { listAggregator, listValidator };
